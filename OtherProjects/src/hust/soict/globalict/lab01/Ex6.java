import java.util.Scanner;
public class Ex6 {
    public static void equation(){
        int num1, num2;
        System.out.println("Enter two numbers: ");
        Scanner sc = new Scanner(System.in);
        num1 = sc.nextInt();
        num2 = sc.nextInt();
        sc.close();

        System.out.println("Your equation is: " + num1 + "x" + "+" + num2 + "=0");
        if(num1 == 0){
            if(num2 == 0){
                System.out.println("Infinite solutions");
            }
            else{
                 System.out.println("No solution");
            }
        }
        else{
            double x = (double) -num2 / num1;
            System.out.println("Unique solution: "+x);
        }
    }

    public static void setOfEquations(){
        int a11, a12, a21, a22, b1, b2;
        int d, d1, d2;
        System.out.println("Enter the coefficients a11, a12, a21, a22, b1 and b2: ");
        Scanner sc = new Scanner(System.in);
        a11 = sc.nextInt();
        a12 = sc.nextInt();
        a21 = sc.nextInt();
        a22 = sc.nextInt();
        b1 = sc.nextInt();
        b2 = sc.nextInt();
        sc.close();

        System.out.println("Your system of equations is: ");
        System.out.println(a11 + "(x1)" + "+" + a12 + "(x2)" + "=" + b1);
        System.out.println(a21 + "(x1)" + "+" + a22 + "(x2)" + "=" + b2);

        d = a11*a22 - a21*a12;
        d1 = b1*a22 - b2*a12;
        d2 = a11*b2 - a21*b1;

        if(d == 0){
            if(d1==0 && d2 == 0){
                System.out.println("The system has infinite solutions");
            }
            else{
                System.out.println("The system has no solution"); 
            }
        }
        else{
            double x1 = (double) d1 / d;
            double x2 = (double) d2 / d;
            System.out.println("The system has a unique solution" + "(" + x1 + "," + x2 +")");
        }
    }

    public static void secondDegreeEquation() {
        double a, b, c;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a: ");
        a = sc.nextDouble();
        System.out.println("Enter b: ");
        b = sc.nextDouble();
        System.out.print("Enter c: ");            
        c = sc.nextDouble();
        sc.close();

        double result = b * b - 4.0 * a * c;

        if (result > 0.0) {
            double r1 = (-b + Math.pow(result, 0.5)) / (2.0 * a);
            double r2 = (-b - Math.pow(result, 0.5)) / (2.0 * a);
            System.out.println("The equation has two distinct roots " + r1 + " and " + r2);
        } 
        else if (result == 0.0) {
            double r1 = -b / (2.0 * a);
            System.out.println("The equation has double root " + r1);
        } 
        else {
            System.out.println("The equation has no solution");
        }
    }
    public static void main(String[] args) {
        System.out.println("What kind of problem do you want to solve:\n1. The first-degree equation with one variable \n2. The system of first-degree equations with two variables \n3. The second-degree equation with one variable");
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        switch (a) {
            case 1:
                equation();
                break;
            case 2:
                setOfEquations();
                break;
            case 3:
                secondDegreeEquation();
                break;
            default:
                System.out.println("Invalid choice");
        }

    }
}