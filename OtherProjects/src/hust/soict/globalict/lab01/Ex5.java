import java.util.Scanner;

public class Ex5 {
    public static void main(String[] args) {
        int num1, num2, sum, difference, product, quotient;
        System.out.println("Enter two numbers: ");
        Scanner sc = new Scanner(System.in);
        num1 = sc.nextInt();
        num2 = sc.nextInt();
        sc.close();
        
        sum = num1 + num2;
        System.out.println("Sum of two numbers: "+sum);

        difference = num1 - num2;
        System.out.println("Difference of two numbers: "+difference);

        product = num1 * num2;
        System.out.println("Product of two numbers: "+product);

        if(num2 == 0){
            System.out.println("Invalid divisor");
        }
        else{
            quotient = num1 / num2;
            System.out.println("Quotient of two numbers: "+quotient);
        }
    }
}
