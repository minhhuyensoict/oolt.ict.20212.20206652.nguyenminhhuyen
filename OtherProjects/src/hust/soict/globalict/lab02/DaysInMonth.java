import java.util.Scanner;

public class DaysInMonth {
    public static void main(String[] args) {
        String arrMonths[][] = { { "January", "Jan", "Jan.", "1" }, { "February", "Feb", "Feb.", "2" },
                { "March", "Mar", "Mar.", "3" }, { "April", "Apr", "Apr.", "4" }, { "May", "May", "May", "5" },
                { "June", "June", "Jun", "6" }, { "July", "July", "Jul", "7" }, { "August", "Aug", "Aug.", "8" },
                { "September", "Sep", "Sep.", "9" }, { "October", "Oct", "Oct.", "10" },
                { "November", "Nov", "Nov.", "11" }, { "December", "Dec", "Dec.", "12" } };
        int days = 0, year = 0, k = 0;
        int monthEntered = 0;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Enter a month: ");
            String month = sc.nextLine();

            for (int i = 0; i < 12; ++i) {
                for (int j = 0; j < 4; ++j) {
                    int result = month.compareTo(arrMonths[i][j]);
                    if (result == 0) {
                        monthEntered = i + 1;
                        k = 1;
                        break;
                    }
                }
            }
        } while (k != 1);
        do {
            System.out.println("Enter a year: ");
            year = sc.nextInt();
        } while (year <= 0);

        sc.close();

        if (monthEntered == 1 || monthEntered == 3 || monthEntered == 5 || monthEntered == 7 || monthEntered == 8
                || monthEntered == 10 || monthEntered == 12) {
            days = 31;
        } else if (monthEntered == 2) {
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
                days = 29;
            } else {
                days = 28;
            }
        } else if (monthEntered == 4 || monthEntered == 6 || monthEntered == 9 || monthEntered == 11) {
            days = 30;
        } else {
            System.out.println("Invalid month.");
        }
        System.out.println(monthEntered + "/" + year + " has " + days + " days\n");
    }
}