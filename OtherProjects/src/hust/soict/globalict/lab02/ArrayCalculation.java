import java.util.Scanner;
import java.util.Arrays;

public class ArrayCalculation {
    public static void main(String[] args) {
        int n, sum = 0;
        float average;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of elements in the array:");
        n = sc.nextInt();
        int a[] = new int[n];
        System.out.println("Enter the elements in the array:");
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
            sum += a[i];
        }

        System.out.println("Your array: ");
        for (int i = 0; i < n; i++) {
            System.out.print(a[i] + "\t");
        }
        System.out.print("\n");

        Arrays.sort(a);

        System.out.println("Your sorted array: ");
        for (int i = 0; i < n; i++) {
            System.out.print(a[i] + "\t");
        }
        System.out.print("\n");

        System.out.println("Sum of array elements: " + sum);
        average = (float) sum / n;
        System.out.println("Average value of array elements: " + average);

        sc.close();
    }
}