import java.util.Scanner;

public class Triangle {
    public static void main(String[] args) {
        System.out.println("Enter a number: ");
        Scanner sc = new Scanner(System.in);
        int rows;
        rows = sc.nextInt();
        sc.close();

        for (int i = 1; i <= rows; ++i) {
            for (int j = 1; j <= rows - i; ++j) {
                System.out.print("  ");
            }

            for (int k = 0; k < 2 * i - 1; k++) {
                System.out.print("* ");
            }

            System.out.println("\n");
        }
    }
}
