import java.util.Scanner;

public class Matrices {
    public static void main(String[] args) {
        int a, b;
        System.out.println("Enter two numbers: ");
        Scanner scan = new Scanner(System.in);
        a = scan.nextInt();
        b = scan.nextInt();
        int number1[][] = new int[a][b];
        int number2[][] = new int[a][b];
        int total[][] = new int[a][b];
        System.out.println("Enter the first matrix: ");
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                number1[i][j] = scan.nextInt();
            }
        }
        System.out.println("Enter the second matrix: ");
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                number2[i][j] = scan.nextInt();
            }
        }
        System.out.println("The result is: ");
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                total[i][j] = number1[i][j] + number2[i][j];
                System.out.print(total[i][j] + "\t");
            }
            System.out.print("\n");
        }
        scan.close();
    }
}
