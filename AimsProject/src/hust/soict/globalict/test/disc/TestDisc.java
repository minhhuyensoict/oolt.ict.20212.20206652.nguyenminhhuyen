package hust.soict.globalict.test.disc;

import hust.soict.globalict.aims.media.DigitalVideoDisc;

public class TestDisc {
    public static void main(String[] args) {
        DigitalVideoDisc disc = new DigitalVideoDisc("Harry Potter"); 
        System.out.println(disc.search("harry"));
        System.out.println(disc.search("abc"));
    }
  
}
