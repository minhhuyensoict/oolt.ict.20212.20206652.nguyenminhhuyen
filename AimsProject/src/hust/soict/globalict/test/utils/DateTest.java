package hust.soict.globalict.test.utils;

import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {
    public static void main(String[] args) {
        var date1 = new MyDate(22,11,2000);
        var date2 = new MyDate(8,11,2002);
        var date3 = new MyDate(8,2,2001);
        MyDate[] dMyDates = {date1, date2, date3};
        System.out.println(DateUtils.compareDate(date1, date2));
        var sortedDate = DateUtils.sortDate(dMyDates);
        for (MyDate date: sortedDate) {
            date.print();
        }

        for (MyDate date: sortedDate) {
            date.printFormat("dd-MMM-yyyy");
        }
    }
}

