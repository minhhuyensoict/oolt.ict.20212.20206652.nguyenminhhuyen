package hust.soict.globalict.aims.utils;

import java.util.ArrayList;
import java.util.Arrays;


public class DateUtils {
    public static int compareDate(MyDate date1, MyDate date2) {
        if (date1.getYear() != date2.getYear()) {
            return date1.getYear() > date2.getYear() ? 1 : -1;
        }
        if (date1.getMonth() != date2.getMonth()) {
            return date1.getMonth() > date2.getMonth() ? 1 : -1;
        }
        if (date1.getDay() != date2.getDay()) {
            return date1.getDay() > date2.getDay() ? 1 : -1;
        }
        return 0;
    }

    public static ArrayList<MyDate> sortDate(MyDate[] dates) {
        var arrayDates = new ArrayList<>(Arrays.asList(dates));
        arrayDates.sort(
            (MyDate date1, MyDate date2) -> DateUtils.compareDate(date1, date2)
        );
        return arrayDates;
    }
}
