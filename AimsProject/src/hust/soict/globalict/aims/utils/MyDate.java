package hust.soict.globalict.aims.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MyDate {
    private int day;
    private int month;
    private int year;

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate() {
        this.day = LocalDateTime.now().getDayOfMonth();
        this.month = LocalDateTime.now().getMonthValue();
        this.year = LocalDateTime.now().getYear();
    }

    public MyDate(String s) throws ParseException {
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(s);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        this.day = cal.get(Calendar.DAY_OF_MONTH);
        this.month = cal.get(Calendar.MONTH) +1;
        this.year = cal.get(Calendar.YEAR);
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if (day < 0 || day > 31) {
            throw new InvalidParameterException("Invalid day");
        } else {
            this.day = day;
        }
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if (month < 0 || month > 12) {
            throw new InvalidParameterException("Invalid month");
        } else {
            this.month = month;
        }
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if (year < 0) {
            throw new InvalidParameterException("Invalid year");
        } else {
            this.year = year;
        }
    }


    public static MyDate accept() throws ParseException, IOException {
        System.out.println("Please enter a date, format: dd/MM/yyyy");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();
        return new MyDate(input);
    }

    public String toString() {
        return this.format("MMMMM dd YYYY");
    }

    public void print() {
        this.printFormat("MMMMM dd YYYY");
    }

    public static void main(String[] args) throws ParseException, IOException {
        MyDate myDate1 = new MyDate();
        myDate1.print();
        MyDate myDate2 = new MyDate(17, 7, 2021);
        myDate2.print();
        MyDate myDate3 = new MyDate("30/04/2022");
        myDate3.print();
    }

    public String format(String format) {
        Calendar myCalendar = new GregorianCalendar(this.year, this.month, this.day);
        Date myDate = myCalendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(myDate);
    }

    public void printFormat(String format) {
        System.out.println(this.format(format));
    }
}