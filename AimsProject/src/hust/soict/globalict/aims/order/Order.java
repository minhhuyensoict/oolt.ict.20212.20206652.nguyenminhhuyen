package hust.soict.globalict.aims.order;

import java.util.ArrayList;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.*;

public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERS = 5;
    private static int nbOrders = 0;
    private static int cur_id = 0;
    private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
    private Media luckyItem = null;
    private MyDate dateOrdered;
    public int id;

    public Media find_with_id(int id) {
        for (Media item : itemsOrdered) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    public void addMedia(Media media) {
        itemsOrdered.add(media);
    }

    public void removeMedia(Media media) {
        itemsOrdered.remove(media);
    }

    public void removeMedia(int id) {
        Media item = find_with_id(id);
        if (item != null)
            itemsOrdered.remove(item);
    }

    public float totalCost() {
        float total_cost = 0;
        Media luckyItem = getALuckyItem();
        for (Media media : itemsOrdered)
            if (media != luckyItem) {
                total_cost += media.getCost();
            }
        return total_cost;
    }

    public Order() {
        nbOrders++;
        if (nbOrders <= MAX_LIMITED_ORDERS) {
            id = Order.cur_id;
            itemsOrdered = new ArrayList<Media>();
            dateOrdered = new MyDate();
            Order.cur_id++;
        } else {
            System.out.println("Order limit reached");
        }
    }

    public static int getnbOrders() {
        return nbOrders;
    }

    public MyDate getDateOrdered() {
        return dateOrdered;
    }

    public void printOrder() {
        System.out.println("***********************Order***********************");
        System.out.println("Date: " + this.dateOrdered.toString());
        System.out.println("Ordered Items:");
        luckyItem = getALuckyItem();
        for (Media item : itemsOrdered) {
            if (item instanceof DigitalVideoDisc) {
                DigitalVideoDisc disc = (DigitalVideoDisc) item;
                System.out.print(
                        String.format("%d. DVD - %s - %s - %d: %.2f$", disc.getId(), disc.getTitle(),
                                disc.getDirector(), disc.getLength(), disc.getCost()));
            }
            if (item instanceof Book) {
                Book book = (Book) item;
                System.out.print(
                        String.format("%d. Book - %s - %s - %d: %.2f$", book.getId(), book.getTitle(),
                                book.getAuthors(), book.getClass(), book.getCost()));
            }
            if (item == luckyItem) {
                System.out.println(" - Lucky item");
            } else System.out.println();
        }
        System.out.println(String.format("Total cost: %f", this.totalCost()));
        System.out.println("***************************************************");
    }

    Media getALuckyItem() {
        if (luckyItem == null)
            luckyItem = this.itemsOrdered.get((int) ((Math.random() * itemsOrdered.size())));
        return luckyItem;
    }
}
