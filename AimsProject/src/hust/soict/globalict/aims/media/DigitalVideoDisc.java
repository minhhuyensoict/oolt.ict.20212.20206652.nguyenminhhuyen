package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Media {
	private String director;
	private int length;
	
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public DigitalVideoDisc(String title) {
		super(title);
	}
	
	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}
	
	public DigitalVideoDisc(String title, String category, String directory) {
		super(title, category);
		this.director = directory;
	}
	
	public DigitalVideoDisc(String title, String category, String directory, int length, float cost) {
		super(title, category, cost);
		this.length = length;
		this.director = directory;
	}
	
	public boolean search(String title) {
		String[] tokenList = title.toLowerCase().split(" ");
		for (String token: tokenList) {
			if(!getTitle().toLowerCase().contains(token))
				return false;
		}
		return true;
	}
}
