package hust.soict.globalict.aims.media;

import java.util.*;

public class Book extends Media {
    private List<String> authors = new ArrayList<String>();

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public Book(String title) {
        super(title);
    }

    public Book(String title, String category) {
        super(title, category);
    }

    public Book(String title, String category, float cost) {
        super(title, category, cost);
    }

    public void addAuthor(String authorName) {
        for (String author : authors) {
            if (Objects.equals(author, authorName))
                return;
        }
        authors.add(authorName);
    }

    public void removeAuthor(String authorName) {
        for (String author : authors) {
            if (Objects.equals(author, authorName))
                authors.remove(author);
            break;
        }
    }
}
