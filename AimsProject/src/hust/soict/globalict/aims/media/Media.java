package hust.soict.globalict.aims.media;

public class Media {

    private static int cur_id;
	private String title;
	private String category;
	private float cost;
	private int id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public Media(String title) {
		this.title = title;
        this.id = Media.cur_id;
        Media.cur_id++;
	}
	
	public Media(String title, String category) {
		this(title);
		this.category = category;
	}
	
	public Media(String title, String category, float cost) {
		this(title, category);
		this.cost = cost;
	}
	
	public Media(String title, String category, float cost, int id) {
		this(title, category, cost);
		this.id = id;
	}

}
