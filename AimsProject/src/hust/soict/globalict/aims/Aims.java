package hust.soict.globalict.aims;

import java.util.Scanner;

import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.order.*;

public class Aims {
    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static void main(String[] args) {
        int choice = 0;
        Scanner input = new Scanner(System.in);
        showMenu();
		Order order = null;
        do {
            choice = input.nextInt();
            input.nextLine();
            switch (choice) {
                case 1:
                    if (Order.getnbOrders() == Order.MAX_LIMITED_ORDERS) {
                        System.out.println("max order will exceed");
                        break;
                    }
                    order = new Order();
                    System.out.println("created new order");
                    break;
                case 2:
                    if (order != null) {
                        System.out.println("enter item type: (book/dvd)");
                        String type = input.nextLine();
                        System.out.println("enter title");
                        String title = input.nextLine();
                        System.out.println("enter category");
                        String category = input.nextLine();
                        System.out.println("enter cost");
                        float cost = input.nextFloat();
                        input.nextLine();
                        Media item;
                        if (type.equals("dvd"))
                            item = new DigitalVideoDisc(title, category);
                        else
                            item = new Book(title, category);
                        item.setCost(cost);
                        order.addMedia(item);
                    } else
                        System.out.println("please create order first");
                    break;
                case 3:
                    if (order != null) {
                        System.out.println("enter id:");
                        int id = input.nextInt();
                        input.nextLine();
                        order.removeMedia(id);
                    } else
                        System.out.println("please create order first");
                    break;
                case 4:
                    if (order != null) {
                        order.printOrder();

                    } else
                        System.out.println("No order has been created!");
                    break;
                case 0:
                    break;
                default:
                    System.out.println("invalid option: " + choice);
                    break;
            }
            showMenu();
        } while (choice != 0);
        input.close();
    }
}
