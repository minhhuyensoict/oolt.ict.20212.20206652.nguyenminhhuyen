import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Game {
    public static ArrayList<Player> players = new ArrayList<>();
    public static Dice[] dices = { new Dice(1), new Dice(2), new Dice(3), new Dice(4) };
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int choice;
        do {
            printMenu();
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    addPlayer();
                    break;
                case 2:
                    play();
                    return;
            }
        } while (choice != 0);

    }

    private static void printMenu() {
        System.out.println("1. Add a player");
        System.out.println("2. Start the game");
        System.out.println("0. Quit");
    }

    private static void addPlayer() {
        if (players.size() < 4) {
            System.out.print("Enter player's name: ");
            String s = sc.next();
            players.add(new Player(s));
        } else {
            System.out.println("Enough player");
        }
    }

    private static void play() {
        Scanner scanner = new Scanner(System.in);
        int num_of_bot = 4 - players.size();
        for (int i = 1; i <= num_of_bot; i++) {
            players.add(new BotPlayer(String.valueOf(i)));
        }
        Referee ref = new Referee();
        for (Player player : players) {
            ref.addPlayer(player);
        }
        while (true) {
            Player player = ref.nextPlayer();
            int num = new Random().nextInt(4);
            Dice dice = dices[num];
            int dots = dice.roll();
            if (player instanceof BotPlayer == false) {
                System.out.println(String.format("Turn of player %s ", player.name));
                System.out.println(String.format("Player %s got dice number %d", player.name, (num + 1)));
                System.out.println("Enter key to roll...");
                String nothing = scanner.nextLine();
                System.out.println(String.format("It's %d dots face ", dots));
            }
            Boolean gameover = ref.calculateScore(player, (num + 1), dots);
            System.out.println();
            if (gameover) {
                return;
            }

        }
    }

}
