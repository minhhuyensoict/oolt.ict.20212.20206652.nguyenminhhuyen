import java.util.ArrayList;

public class Referee {
    public ArrayList<Player> players = new ArrayList<>();
    int number_of_player = 0;
    int cur_player = 0;

    public void addPlayer(Player player) {
        if (number_of_player < 4) {
            this.players.add(player);
            number_of_player++;
        } else {
            System.out.println("Enough player!");
        }
    }

    public Player nextPlayer() {
        Player player = this.players.get(cur_player);
        cur_player = (cur_player + 1) % number_of_player;
        return player;
    }

    public boolean calculateScore(Player player, int dice, int score) {
        int new_score = player.getScore() + score;
        if (player instanceof BotPlayer) {
            System.out.println(String.format("Bot player %s now has scored: %d", player.name, player.score));
            if (new_score > 21) {
                player.setScore(0);
                System.out.println(String.format(
                        "After rolled face with %d dots of dice number %d, bot player %s has 0 point! ", score, dice,
                        player.name));
            } else if (new_score == 21) {
                System.out.println(String.format(
                        "After rolled face with %d dots of dice number %d, bot player %s has 21 points! ", score, dice,
                        player.name));
                System.out.println(String.format("Bot %s has won!", player.name));
                for (Player player_ : players) {
                    if (player_ instanceof BotPlayer && player_ != player) {
                        BotPlayer botPlayer = (BotPlayer) player_;
                        botPlayer.lose();
                    }
                }
                return true;
            } else {
                player.setScore(new_score);
                System.out.println(String.format(
                        "After rolled face with %d dots of dice number %d, bot player %s has %d point! ", score, dice,
                        player.name, new_score));
            }
        } else {
            System.out.println(String.format("Player %s now has scored: %d", player.name, player.score));
            if (new_score > 21) {
                player.setScore(0);
                System.out.println(
                        String.format("After rolled face with %d dots, player %s has 0 point! ", score, player.name));
            } else if (new_score == 21) {
                System.out.println(
                        String.format("After rolled face with %d dots, player %s has 21 points! ", score, player.name));
                System.out.println(String.format("%s has won!", player.name));
                for (Player player_ : players) {
                    if (player_ instanceof BotPlayer && player_ != player) {
                        BotPlayer botPlayer = (BotPlayer) player_;
                        botPlayer.lose();
                    }
                }
                return true;
            } else {
                player.setScore(new_score);
                System.out.println(String.format("After rolled face with %d dots, player %s has %d point! ", score,
                        player.name, new_score));
            }
        }
        return false;
    }
}
