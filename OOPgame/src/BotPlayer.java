public class BotPlayer extends Player {

    public BotPlayer(String name) {
        super(name);
    }

    public String[] expressions = { "Oh no!", "I lose again!", "Just lucky!", "Again!" };

    public void lose() {
        System.out.println(String.format("Bot player %s has lost!", name));
        System.out.println(expressions[(Integer.valueOf(name)) - 1]);
    }

}
