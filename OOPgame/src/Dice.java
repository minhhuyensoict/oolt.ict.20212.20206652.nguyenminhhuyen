import java.util.Random;

public class Dice {
    Integer[] choices = { 1, 1, 1, 1,
            2, 2, 2, 2,
            3, 3, 3, 3,
            4, 4, 4, 4,
            5, 5, 5, 5,
            6, 6, 6, 6,
            null };

    public Dice(int face) {
        this.choices[24] = face;
    }

    public int roll() {
        int face = new Random().nextInt(choices.length);
        return this.choices[face];
    }
}
